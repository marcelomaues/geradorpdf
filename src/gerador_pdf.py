from docx.enum.text import WD_ALIGN_PARAGRAPH, WD_TAB_ALIGNMENT
from docx.enum.table import WD_ALIGN_VERTICAL
from docx.enum.section import WD_ORIENT
from docx.shared import RGBColor
from docx import Document
from docx.shared import Inches

import docx


class CriaDoc:
    def __init__(self, json_file: dict):
        self.json_file: dict = json_file
        self.document: Document = Document()        
         
    def FormatoDocx(self):
        """
        Formatacao do doc, paisagem ou retrato
        """
        
        formato = self.json_file["Formato"]
    
        if formato == 'Paisagem':
            novaAltura = Inches(8.3)
            novaLargura = Inches(11.7)
            section = self.document.sections[-1]
            section.orientation = WD_ORIENT.LANDSCAPE
            section.page_width = novaLargura
            section.page_heigth = novaAltura
            self.document.add_picture('Venice.png', width = Inches(9.15))
            paragraph = self.document.add_paragraph()
            paragraph.paragraph_format.line_spacing = 5.0
        elif formato == 'Retrato':
            novaAltura = Inches(11.7)
            novaLargura = Inches(8.3)
            section = self.document.sections[-1]
            section.orientation = WD_ORIENT.LANDSCAPE
            section.page_width = novaLargura
            section.page_heigth = novaAltura
            section = self.document.sections[-1]
            section.orientation = WD_ORIENT.PORTRAIT
            self.document.add_picture('Venice.png', width = Inches(5.75))
            paragraph = self.document.add_paragraph()
            paragraph.paragraph_format.line_spacing = 5.0

    def Rodape(self):
        """
        Nota de rodape, com o nome da empresa, do que é o documento e numero de paginas
        """
        esq =  self.json_file["Detalhes"]["Rodape"]["Esq"]
        meio =  self.json_file["Detalhes"]["Rodape"]["Meio"]

        section = self.document.sections[0]
        Rodape = section.footer
        paragraph = Rodape.paragraphs[0]
        paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER
        paragraph .text = "{}\t {} \t  Pagina 1 de 1".format(esq,meio)
        paragraph.paragraph_format.alignment = WD_TAB_ALIGNMENT.CENTER
        
    def TextoDeCima(self):
        """
        Texto que aparece em cima da tabela, com nome do assessor e aliquota
        """
        linhaSupEsq = self.json_file["Detalhes"]["LinhaSupEsq"]
        linhaSupDir =  self.json_file["Detalhes"]["LinhaSupDir"]
        sec = self.document.sections[0]
        margin_end = docx.shared.Inches(sec.page_width.inches - (sec.left_margin.inches + sec.right_margin.inches))
        paragraph = self.document.add_paragraph("{}\t{}".format(linhaSupEsq,linhaSupDir))
        tab_stops = paragraph.paragraph_format.tab_stops
        tab_stops.add_tab_stop(margin_end, docx.enum.text.WD_TAB_ALIGNMENT.RIGHT)

        paragraph.paragraph_format.line_spacing = 5.0

    def CriaTabela(self): 
        """
        cria a tabela e adiciona os valores
        """
        latent_styles = self.document.styles.latent_styles
        latent_styles.default_to_locked = True
        latent_style = latent_styles.add_latent_style('Medium Shading 1 Accent 1')
        latent_style.hidden = False
        latent_style.priority = 1
        latent_style.quick_style = True
        latent_style = latent_styles['Medium Shading 1 Accent 1']
        tabela: list = self.json_file["Detalhes"]["Tabela"]["Conteudo"]
        cabecalho: list = self.json_file["Detalhes"]["Tabela"]["Cabecalho"]
    
        records: list = []
        for row in tabela:
            records.append(tuple(row))
        
        TamanhoCabecalho =len(cabecalho)
        table = self.document.add_table(rows=1, cols=TamanhoCabecalho, style = 'Medium Shading 1 Accent 1')
        table.autofit = True
        table.style.name
        'Medium Shading 1 Accent 1'
        table.style.font.color.rgb = RGBColor(0x0, 0x0, 0x0)
        hdr_cells = table.rows[0].cells
      
        i = 0
        while TamanhoCabecalho > i:
            hdr_cells[i].text = cabecalho[i] 
            hdr_cells[i].alignment =  WD_ALIGN_VERTICAL.CENTER
            i += 1        
        for tabela in records:
            row_cells = table.add_row().cells
            rowi = 0    
            for coluna in tabela:    
                row_cells[rowi].text = coluna
                row_cells[rowi].alignment =  WD_ALIGN_VERTICAL.CENTER
                rowi += 1
                