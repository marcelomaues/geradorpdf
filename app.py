from docx2pdf import convert
from flask import Flask,send_file
from flask_restful import Resource, Api
from flask import request
from src.gerador_pdf import CriaDoc

import pythoncom

#from docx.enum.table import WD_CELL_ALIGN_VERTICAL

#python-docx e docx2pdf são as bibliotecas
#pip install waitress

app = Flask(__name__)
app.config['documento'] = "C:\Projetos\GeradorPDF"
api = Api(app)

caminho = "C:\Projetos\GeradorPDF\teste.pdf"
class APIPost(Resource):

    @app.route("/uploads/<path:name>", methods=["POST"])
    def post(name):
        response_json = request.get_json()

        doc = CriaDoc(response_json) 
        doc.FormatoDocx()
        doc.TextoDeCima()
        doc.CriaTabela()
        doc.Rodape()
        doc.document.save('temp.docx')
        pythoncom.CoInitialize()
        convert('temp.docx')
        p = "temp.pdf"
        return send_file(p,as_attachment = True)    

if __name__ == '__main__':
    api.add_resource(APIPost, '/APIPost')
    app.run(host='0.0.0.0')